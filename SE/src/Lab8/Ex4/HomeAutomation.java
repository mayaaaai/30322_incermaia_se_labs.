package Lab8.Ex4;

import java.util.ArrayList;
import java.util.Random;

public class HomeAutomation {
    public static void main(String[] args) {
        ControlUnit controlUnit = ControlUnit.maker();
        
        Home h = new Home() {
            protected void setValueInEnvironment(Event event) {
                controlUnit.eventHandler(event);
                System.out.println("New event in environment " + event);
            }

            protected void controllStep() {
                System.out.println("Control step executed \n");
            }
        };
        h.simulate();
    }
}

abstract class Home {
    private Random r = new Random();
    private final int SIMULATION_STEPS = 20;

    protected abstract void setValueInEnvironment(Event event);

    protected abstract void controllStep();

    private Event getHomeEvent() {
        // randomly generate a new event;
        int k = r.nextInt(100);
        if (k < 30)
            return new NoEvent();
        else if (k < 60)
            return new FireEvent(r.nextBoolean());
        else
            return new TemperatureEvent(r.nextInt(50));
    }

    public void simulate() {
        int k = 0;
        while (k < SIMULATION_STEPS) {
            Event event = this.getHomeEvent();
            setValueInEnvironment(event);
            controllStep();

            try {
                Thread.sleep(300);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

            k++;
        }
    }
}

class ControlUnit {
    private static ControlUnit singleton = null;

    private ControlUnit() {
    };

    private static final double MAX_TEMP = 30;
    private static final double MIN_TEMP = 15;

    FireSensor f1 = new FireSensor(true);
    FireSensor f2 = new FireSensor(false);
    private final ArrayList<FireSensor> fireSensors = new ArrayList<FireSensor>();
    private TempSensor tempSensor = new TempSensor(20);

    private GsmUnit gsmUnit = new GsmUnit();
    private AlarmUnit alarmUnit = new AlarmUnit();
    private HeatingUnit heatingUnit = new HeatingUnit();
    private CoolingUnit coolingUnit = new CoolingUnit();

    public static ControlUnit maker() {
        if (singleton == null)
            singleton = new ControlUnit();
        return singleton;
    }

    public void eventHandler(Event event) {
        if (event instanceof FireEvent fireEvent) {
            if (fireEvent.isSmoke()) {
                alarmUnit.setRing(true);
                gsmUnit.callOwner(event);
            }
        } else if (event instanceof TemperatureEvent temperatureEvent) {
            if (temperatureEvent.getValue() > MAX_TEMP) {
                coolingUnit.activate();
                gsmUnit.callOwner(event);
            } else if (temperatureEvent.getValue() < MIN_TEMP) {
                heatingUnit.activate();
                gsmUnit.callOwner(event);
            } else if(tempSensor.temp == 20){
                System.out.println("Temperature ok");
            }
        }
    }
}

class AlarmUnit {
    private boolean ring = false;

    public boolean isRinging() {
        return ring;
    }

    public void setRing(boolean ring) {
        this.ring = ring;
    }
}

class HeatingUnit {
    public boolean active = false;

    public void activate() {
        this.active = true;
        System.out.println("Heating unit activated!");
    }

    public boolean getActive() {
        return active;
    }
}

class CoolingUnit {
    private boolean active = false;

    public void activate() {
        this.active = true;
        System.out.println("Cooling unit activated!");
    }

    public boolean getActive() {
        return active;
    }
}

class GsmUnit {
    public void callOwner(Event event) {
        System.out.println("Calling owner to inform about: " + event);
    }
}

class FireSensor {

    boolean isFire = false;

    public FireSensor(boolean isFire) {
        this.isFire = isFire;
    }

    public boolean isFire() {
        return isFire;
    }

    public void setFire(boolean isFire) {
        this.isFire = isFire;
    }

}

class TempSensor {
    int temp;

    public TempSensor(int temp) {
        this.temp = temp;
    }

    public void setTemp(){
        this.temp = new Random().nextInt(100);
    }

    public int getTemp() {
        return temp;
    }

}

abstract class Event {
    EventType type;

    Event(EventType type) {
        this.type = type;
    }

    EventType getType() {
        return type;
    }
}

class TemperatureEvent extends Event {
    private int value;

    TemperatureEvent(int value) {
        super(EventType.TEMPERATURE);
        this.value = value;
    }

    int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "TemperatureEvent{" + "value=" + value + '}';
    }

}

class FireEvent extends Event {
    private boolean smoke;

    FireEvent(boolean smoke) {
        super(EventType.FIRE);
        this.smoke = smoke;
    }

    boolean isSmoke() {
        return smoke;
    }

    @Override
    public String toString() {
        return "FireEvent{" + "smoke=" + smoke + '}';
    }
}

class NoEvent extends Event {
    NoEvent() {
        super(EventType.NONE);
    }

    @Override
    public String toString() {
        return "NoEvent{}";
    }
}

enum EventType {
    TEMPERATURE, FIRE, NONE;
}
