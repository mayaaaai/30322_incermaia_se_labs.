package Lab9.Ex5;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.*;

class Controller {

    public String stationName;
    ArrayList<Controller> neighbourController = new ArrayList<Controller>();

    //storing station train track segments
    ArrayList<Segment> list = new ArrayList<Segment>();

    public Controller(String gara) {
        stationName = gara;
    }

    public Controller(){}

    void setNeighbourController(Controller v){
        neighbourController.add(v);
    }

    void addControlledSegment(Segment s){
        list.add(s);
    }

    /**
     * Check controlled segments and return the id of the first free segment or -1 in case there is no free segment in this station
     *
     * @return
     */
    int getFreeSegmentId(){
        for(Segment s:list){
            if(s.hasTrain()==false)
                return s.id;
        }
        return -1;
    }

    void controlStep(){
        //check which train must be sent

        for(Segment segment:list){
            if(segment.hasTrain()){
                Train t = segment.getTrain();
                for(int i=0;i<this.neighbourController.size();i++)
                    if(t.getDestination().equals(neighbourController.get(i).stationName)){
                        //check if there is a free segment
                        int id = neighbourController.get(i).getFreeSegmentId();
                        if(id==-1){
                            System.out.println("Trenul +"+t.name+"din gara "+stationName+" nu poate fi trimis catre "+neighbourController.get(i).stationName+". Nici un segment disponibil!");
                            return;
                        }
                        //send train
                        System.out.println("Trenul "+t.name+" pleaca din gara "+stationName +" spre gara "+neighbourController.get(i).stationName);
                        segment.departTRain();
                        neighbourController.get(i).arriveTrain(t,id);
                    }

            }
        }//.for

    }//.


    public void arriveTrain(Train t, int idSegment){
        for(Segment segment:list){
            //search id segment and add train on it
            if(segment.id == idSegment)
                if(segment.hasTrain()==true){
                    System.out.println("CRASH! Train "+t.name+" colided with "+segment.getTrain().name+" on segment "+segment.id+" in station "+stationName);
                    return;
                }else{
                    System.out.println("Train "+t.name+" arrived on segment "+segment.id+" in station "+stationName);
                    segment.arriveTrain(t);
                    return;
                }
        }

        //this should not happen
        System.out.println("Train "+t.name+" cannot be received "+stationName+". Check controller logic algorithm!");

    }


    public String displayStationState(){
        System.out.println("=== STATION "+stationName+" ===");
        String string = new String();
        for(Segment s:list){
            if(s.hasTrain()) {
                System.out.println("|----------ID=" + s.id + "__Train=" + s.getTrain().name + " to " + s.getTrain().destination + "__----------|");
                string=string.concat("|----------ID=" + s.id + "__Train=" + s.getTrain().name + " to " + s.getTrain().destination + "__----------|");
                string=string.concat("\n");
            }
            else {
                System.out.println("|----------ID=" + s.id + "__Train=______ catre ________----------|");
                string=string.concat("|----------ID=" + s.id + "__Train=______ catre ________----------|");
                string=string.concat("\n");
            }
        }
        //System.out.println(string);
        return string;
    }
}


class Segment{
    int id;
    Train train;

    Segment(int id){
        this.id = id;
    }

    boolean hasTrain(){
        return train!=null;
    }

    Train departTRain(){
        Train tmp = train;
        this.train = null;
        return tmp;
    }

    void arriveTrain(Train t){
        train = t;
    }

    Train getTrain(){
        return train;
    }
}

class Train{
    String destination;
    String name;

    public Train(String destinatie, String nume) {
        super();
        this.destination = destinatie;
        this.name = nume;
    }

    String getDestination(){
        return destination;
    }

}
