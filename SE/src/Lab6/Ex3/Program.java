package Lab6.Ex3;
import Lab6.Ex1.BankAccount;


public class Program {
    public static void main(String[] args) {
        var bank = new Bank();
        bank.addAccount(
                new BankAccount("Maia Incer", 64),
                new BankAccount("Carla Puiu", 123),
                new BankAccount("Ilinca Puiu", 0),
                new BankAccount("Mara Puiu", 88),
                new BankAccount("Buze Puiu", 63),
                new BankAccount("Oana Puiu", 2)
        );

        System.out.println("== Bank Accounts ==");
        bank.printAccounts();

        System.out.println("== Bank Accounts ($100 - $200) ==");
        bank.printAccounts(100, 200);

        System.out.println("== Maia Incer's Bank Account ==");
        var account = bank.getAccount("mayaaaai");
        if (account.isPresent()) {
            System.out.println(account.orElseThrow());
        }
    }
}

