package Lab6.Ex1;
import java.util.Objects;

public class BankAccount implements Comparable<BankAccount> {

    private String Owner;
    private double balance;


    public BankAccount(String Owner, double balance){
        this.Owner=Owner;
        this.balance=balance;
    }

    public void withdraw(double amount) throws IllegalAccessException{
        if(amount<0){
            throw new IllegalAccessException("The withdrawn cannot be lower than 0");
        }
        if(balance<=amount){
            throw new IllegalAccessException("There is noy enough money in your bank account to complete the withdrawal!");

        }
        this.balance=this.balance-amount;
    }

    public void deposit(double amount) throws IllegalAccessException{
        if(amount<0){
            throw new IllegalAccessException("The deposited amount cannot be lower than 0!");
        }
        else this.balance=this.balance+amount;
    }


    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getOwner() {
        return Owner;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        var that = (BankAccount) o;
        return Double.compare(that.balance, balance) == 0 && Owner.equals(that.Owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Owner, balance);
    }

    @Override
    public String toString() {
        return "Account belonging to " + Owner + " ($" + balance + ")";
    }

    @Override
    public int compareTo(BankAccount o) {
        return (int)(this.getBalance() - o.getBalance());
    }
}




