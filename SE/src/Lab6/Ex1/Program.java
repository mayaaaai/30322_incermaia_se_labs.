package Lab6.Ex1;


public class Program {
    public static void main(String[] args) {
        var myAccount = new BankAccount("Maia Incer", 100);
        var CarlasAccount = new BankAccount("Martin Carla", 123);
        var MarasAccount = new BankAccount("Mara Martonos", 10970);

        System.out.println(myAccount + " is the same as " + CarlasAccount + ": " + myAccount.equals(CarlasAccount));
        System.out.println(myAccount + " is the same as " + MarasAccount + ": " + myAccount.equals(MarasAccount));
    }
}

