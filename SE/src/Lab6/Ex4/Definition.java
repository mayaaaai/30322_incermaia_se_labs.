package Lab6.Ex4;

public class Definition {
    private final String description;

    public Definition(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}

