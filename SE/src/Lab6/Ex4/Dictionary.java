package Lab6.Ex4;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class Dictionary {
    private final HashMap<Word, Definition> definitions = new HashMap<>();

    public void addWord(Word word, Definition definition) {
        this.definitions.put(word, definition);
    }

    public Definition getDefinition(Word word) {
        return definitions.get(getListWordObject(word));
    }

    public Set<Word> getAllWords() {
        return definitions.keySet();
    }

    public Collection<Definition> getDefinitions() {
        return definitions.values();
    }

    public void removeWord(Word word) {
        this.definitions.remove(getListWordObject(word));
    }

    private Word getListWordObject(Word word) {
        return definitions.keySet().stream().filter(w -> w.equals(word)).findFirst().orElseThrow();
    }
}
