package Lab3;

public class MyPoint {
    private double _x;
    private double _y;

    public MyPoint() {
        this._x = 0;
        this._y = 0;
    }

    public MyPoint(double x, double y) {
        this._x = x;
        this._y = y;
    }

    public double getX() {
        return _x;
    }

    public void setX(double x) {
        this._x = x;
    }

    public double getY() {
        return _y;
    }

    public void setY(double y) {
        this._y = y;
    }

    public void setXY(double x, double y) {
        this._x = x;
        this._y = y;
    }

    @Override
    public String toString() {
        return "(" + _x + ", " + _y + ")";
    }

    public double calculateDistance(double toX, double toY) {
        return Math.sqrt((toX - this.getX()) * (toY - this.getY()) + (toX - this.getX()) * (toX - this.getX()));
    }

    public double calculateDistance(MyPoint to) {
        return this.calculateDistance(to.getX(), to.getY());
    }


    public static void main(String[] args) {
        var firstPoint = new MyPoint();
        var secondPoint = new MyPoint(7.0, 52.2);
        System.out.println("Distance from " + firstPoint + " to " + secondPoint + " is " + firstPoint.calculateDistance(secondPoint));
        System.out.println("Distance from " + firstPoint + " to (66.6, 77.7) is " + firstPoint.calculateDistance(66.6, 77.7));
    }}