package Lab3;

public class Robot {
        int x;

        public Robot() {
            this.x = 1;
        }

        public void change(int k) {
            if (k >= 1) {
                this.x += k;
            }
        }

        @Override
        public String toString() {
            return "Robot { x = " + x + " }";
        }

    public static void main(String[] args) {
        var robot = new Robot();
        robot.change(123);
        System.out.println(robot);
    }
}

