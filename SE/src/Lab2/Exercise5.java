package Lab2;
import java.util.Random;

public class Exercise5 {
    public static void BubbleSort(int[] a){

        int n=a.length;
        for(int i=0; i<=n; i++)
            for(int j=0; j<=n-i; j++)
                if(a[j]>a[j+1]) //facem swap
                { int aux =a[j];
                    a[j]=a[j+1];
                    a[j+1]=aux;
    }
}
    public static void main(String[] args){
        Random rand = new Random();
        int[] a = new int[10];
        for (int i = 0; i < a.length; i++) {
            a[i] = rand.nextInt(100000);
        }
        System.out.print("a: ");

        for (int i : a) {
            System.out.print(i + " ");
        }

        BubbleSort(a);
        System.out.print("\nNew a:");
        for (int i : a) {
            System.out.print(i + " ");
        }


    }

}
