package Lab2;
import java.util.Scanner;

public class Exercise6 {

    static int recursiveFactorial(int n){
        if (n <= 2) {
            return n;
        }
        return n * recursiveFactorial(n - 1);
    }

    static int nonrecursiveFactorial(int n){
        int factorial=1;
        for(int i=1; i<=n; i++){
            factorial=factorial*i;
        }
        return factorial;
    }

    public static void main(String[] args){
        Scanner read = new Scanner(System.in);
        int n= read.nextInt();

        System.out.println("With the recursive method: "+recursiveFactorial(n));
        System.out.println("With the non-recursive method: " +nonrecursiveFactorial(n));
    }
}
