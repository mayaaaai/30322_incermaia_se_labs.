package Lab7.Ex4;

import java.io.*;
import java.util.LinkedList;

public class CarFactory {
    public CarFactory(){}
    static LinkedList<Car> cars = new LinkedList<Car>();

    Car createCar(String model, double price) {
        Car c = new Car(model, price);
        System.out.println("Car successfully created!");
        cars.add(c);
        return c;
    }

    void printCars()
    {
        for(Car c : cars)
        {
            System.out.println(c);
        }
    }

    void saveCar(String fileName) {
        try {
            ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(fileName));
            o.writeObject(this);
            System.out.println("Car saved in a file!");
        } catch (IOException e) {
            System.out.println("The car cannot be saved in a file!");
            e.printStackTrace();
        }
    }

    static Car loadCar(String fileName) throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
        Car c = (Car) in.readObject();
        return c;
    }
}