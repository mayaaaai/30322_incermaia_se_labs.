package Lab7.Ex4;

import java.io.IOException;

public class Test {
    public static void main(String[] args) {
        //test code
        CarFactory carFactory = new CarFactory();
        Car c1 = carFactory.createCar("Opel", 10);
        Car c2 = carFactory.createCar("Audi", 211000);
        Car c3 = carFactory.createCar("Ford", 342000);

        c1.saveCar("BMW.txt");
        c2.saveCar("Audi.txt");
        c3.saveCar("Volkswagen.txt");
        System.out.println("\n");

        Car.readCar("BMW.txt");
        System.out.println("\n");

        // load
        try {
            Car c4 = Car.loadCar("BMW.txt");
            System.out.println(c4);
        }catch (IOException e)
        {
            e.printStackTrace();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }

        carFactory.printCars();


    }
}