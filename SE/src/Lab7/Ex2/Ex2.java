package Lab7.Ex2;

public class Ex2 {

    public static void main(String[] args) {
        String fileName = "data.txt";
        String character = args[0];
        int count = 0;
        try {
            java.io.File file = new java.io.File(fileName);
            java.io.FileReader fileReader = new java.io.FileReader(file);
            java.io.BufferedReader bufferedReader = new java.io.BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) == character.charAt(0)) {
                        count++;
                    }
                }
            }
            bufferedReader.close();
        } catch (Exception e) {
            System.out.println("Error reading file '" + fileName + "'");
        }
        System.out.println("The character '" + character + "' appears " + count + " times in the file.");
    }

}
