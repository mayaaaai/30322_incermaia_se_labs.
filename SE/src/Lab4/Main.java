package Lab4;
import Lab4.Circle;
import Lab4.Rectangle;


public class Main {
    public static void main(String[] args) {
        var circle = new Circle(10);
        var rectangle = new Rectangle(5, 6);
        var square = new Square(8);

        System.out.println(circle + " Area: " + circle.getArea() + " Perimeter: " + circle.getPerimeter());
        System.out.println(rectangle + " Area: " + rectangle.getArea() + " Perimeter: " + rectangle.getPerimeter());
        System.out.println(square + " Area: " + square.getArea() + " Perimeter: " + square.getPerimeter());
    }
}