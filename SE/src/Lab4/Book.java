package Lab4;
import Lab4.Author;


public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int quantityInStock;

    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.quantityInStock = 0;
    }

    public Book(String name, Author[] authors, double price, int quantityInStock) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.quantityInStock = quantityInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    @Override
    public String toString() {
        return "'" + getName() + "'" + " by " + getAuthors().length + " authors.";
    }

    public void printAuthors() {
        for (var author : getAuthors()) {
            System.out.println(author);
        }
    }
    public static void main(String[] args) {

        var book = new Book("Warrior of Light", new Author[] {
                new Author("Franz Kafka", "franz.kafka@net.com", 'm'),
                new Author("Paulo Coelho", "paulocoelho@yahooo.com", 'm')
        }, 87.87);
        System.out.println(book);
    }
}

