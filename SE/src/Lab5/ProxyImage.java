package Lab5;

public class ProxyImage implements Image {
    private Image realImage;
    private final String fileName;
    private final String type;

    public ProxyImage(String fileName, String type) {
        this.fileName = fileName;
        this.type = type;
    }

    @Override
    public void display() {
        if(realImage == null) {
            if (type.equalsIgnoreCase("rotated")) {
                realImage = new RealImage(fileName);
            } else if (type.equalsIgnoreCase("real")) {
                realImage = new RotatedImage(fileName);
            }
        }

        if (realImage != null) {
            realImage.display();
        }
    }
}
