package Lab5;

import java.text.MessageFormat;

public class Controller {
    public static int TIME_SPAN = 20;

    public TemperatureSensor tempSensor;
    public LightSensor lightSensor;

    public Controller() {
        tempSensor = new TemperatureSensor();
        lightSensor = new LightSensor();
    }

    public void control() throws InterruptedException {
        System.out.println(MessageFormat.format("Reading data from sensors for {0} seconds:", TIME_SPAN));

        for (var i = 0; i < TIME_SPAN; i++) {
            System.out.println(MessageFormat.format("[Second {0}] Temperature sensor: {1}", i + 1, this.tempSensor.readValue()));
            System.out.println(MessageFormat.format("[Second {0}] Light sensor sensor: {1}", i + 1, this.lightSensor.readValue()));
            Thread.sleep(1000);
        }
    }
}