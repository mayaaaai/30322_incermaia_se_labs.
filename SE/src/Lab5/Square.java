package Lab5;

public class Square extends Rectangle {
    public Square() {
        super();
    }

    public Square(double side) {
        super(side, side);
    }

    public Square(double side, String colour, boolean filled) {
        super(side, side, colour, filled);
    }

    public double getSide() {
        return getWidth();
    }

    public void setSide(double side) {
        super.setWidth(side);
        super.setLength(side);
    }

    @Override
    public void setLength(double length) {
        setSide(length);
    }

    @Override
    public void setWidth(double width) {
        setSide(width);
    }

    @Override
    public String toString() {
        return "Square { " +
                "side = " + getSide() +
                ", colour = '" + getColour() + '\'' +
                ", filled = " + isFilled() +
                ", area = " + getArea() +
                ", perimeter = " + getPerimeter() +
                " }";
    }
}

