package Lab11.Ex1;



import javax.swing.JFrame;
import javax.swing.JTextArea;

public class TemperatureSensor extends Thread {
    JFrame frame;
    JTextArea textArea;

    public TemperatureSensor(JFrame frame, JTextArea textArea) {
        this.frame = frame;
        this.textArea = textArea;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            textArea.append("Sensor: " + System.currentTimeMillis() + "\n");
            frame.repaint();
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Sensor");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JTextArea textArea = new JTextArea();
        frame.add(textArea);
        frame.setVisible(true);
        TemperatureSensor sensor = new TemperatureSensor(frame, textArea);
        sensor.start();
    }

}

