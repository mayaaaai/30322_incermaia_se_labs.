package Lab10.Ex3;

public class Test {
    public static void main(String[] args) {
        Counter c1 = new Counter(100, null);
        Counter c2 = new Counter(100, 200, c1);

        c1.start();
        c2.start();
    }

}