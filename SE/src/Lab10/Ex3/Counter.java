package Lab10.Ex3;

public class Counter extends Thread {

    int count = 1;
    int max;
    Thread t;

    public Counter(int max, Thread t) {
        this.max = max;
        this.t = t;
    }

    public Counter(int start, int max, Thread t) {
        this.count = start;
        this.max = max;
        this.t = t;
    }

    public void run() {

        if (t != null) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        while (count <= max) {
            System.out.println(count);
            count++;
        }

    }
}